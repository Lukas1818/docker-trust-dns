FROM rust:slim AS builder
ARG TARGETARCH
SHELL ["/bin/bash", "-uo", "pipefail", "-c"]

RUN case ${TARGETARCH} in arm)TARGET="armv7-unknown-linux-musleabihf" ;; arm64)TARGET="aarch64-unknown-linux-musl" ;; 386)TARGET="i686-unknown-linux-musl" ;; amd64)TARGET="x86_64-unknown-linux-musl" ;; esac \
 && rustup target add "$TARGET"

RUN apt-get update && apt-get install -y \
  musl-tools \
  && rm -rf /var/lib/apt/lists/*


Run case ${TARGETARCH} in arm)TARGET="armv7-unknown-linux-musleabihf" ;; arm64)TARGET="aarch64-unknown-linux-musl" ;; 386)TARGET="i686-unknown-linux-musl" ;; amd64)TARGET="x86_64-unknown-linux-musl" ;; esac \
 && cargo install --locked --root /trust-dns --features dns-over-rustls,dns-over-https-rustls --target "$TARGET" -- trust-dns
Run ls /trust-dns/bin/named



FROM scratch

COPY --from=builder /trust-dns/bin/named /named

ENTRYPOINT ["/named"]
